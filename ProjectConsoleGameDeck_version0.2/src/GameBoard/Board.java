package GameBoard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Board {
	// Board size, dimentions and ladders and snakes
	private final int ROWS = 10;
	private final int COLS = 10;
	private final int NUM_SNAKES = 8;
	private final int NUM_LADDERS = 8;
	
	private int[][] gameBoard;
	private int[][] snakes;
	private int[][] ladders;
	
	// Player position
	Map<Player, Integer> playerPositions;
	
	public Board(List<Player> players) {
		this.playerPositions = new HashMap<Player, Integer>();
		for (Player player : players) {
			this.playerPositions.put(player, 0);
		}
		// ROWS X COLS board
		gameBoard = new int[ROWS][COLS];
		for (int row = 0; row < ROWS; row++) {
			for (int col = 0; col < COLS; col++) {
				gameBoard[row][col] = row*ROWS + col + 1;
			}
		}
		// The bellow code sets up the snakes and the ladders
		setSnakes();
		setLadders();
	}
	public boolean movePlayer(Player player, int value) {
		// New position
		int position = playerPositions.get(player);
		position += value;
		
		if (position >= 100) {
			playerPositions.put(player,  100);
			return true;
			
			// ^^ The above code states that if the new position is 100 or above, the player will win
		}else {
			// If the new position is less than 100 check if the new position is at the snakes mouth
			for (int idx = 0; idx < NUM_SNAKES; idx++) {
				if  (snakes[idx][0] == position) {
					position = snakes[idx][1];
					playerPositions.put(player,  position);
					// ^^ The above code moves player to the bottom of the snake
					System.out.println("Noooo! " + player + "takes snake from " + snakes[idx][0] + " to " + snakes [idx][1]);
					return false;
				}
			}
			
			// The below code will check if the players position is at the starting point of a ladder
			for (int idx = 0; idx < NUM_LADDERS; idx++) {
				if (ladders[idx][0] == position) {
					position = ladders[idx][1];
					playerPositions.put(player,  position);
					System.out.println("Yesssss! " + player + " takes ladder from " + ladders[idx][0] + " to " + ladders[idx][1]);
					return false;
				}
			}
			// If the player is not on a snake/ladder, it will just update
			playerPositions.put(player,  position);
			return false;
		}
	}
	// The below code will set the snakes on the board
	
	private void setSnakes() {
		snakes = new int[NUM_SNAKES][2];
		snakes[0][0] = 17;
		snakes[0][1] = 7;
		snakes[1][0] = 54;
		snakes[1][1] = 34;
		snakes[2][0] = 62;
		snakes[2][1] = 19;
		snakes[3][0] = 64;
		snakes[3][1] = 60;
		snakes[4][0] = 87;
		snakes[4][1] = 24;
		snakes[5][0] = 93;
		snakes[5][1] = 73;
		snakes[6][0] = 95;
		snakes[6][1] = 75;
		snakes[7][0] = 99;
		snakes[7][1] = 78;
	}
	
	
	// The below code will set the ladders on the board
	
	private void setLadders() {
		ladders = new int[NUM_LADDERS][2];
		ladders[0][0] = 4;
		ladders[0][1] = 14;
		ladders[1][0] = 9;
		ladders[1][1] = 31;
		ladders[2][0] = 20;
		ladders[2][1] = 38;
		ladders[3][0] = 28;
		ladders[3][1] = 84;
		ladders[4][0] = 40;
		ladders[4][1] = 59;
		ladders[5][0] = 51;
		ladders[5][1] = 67;
		ladders[6][0] = 63;
		ladders[6][1] = 81;
		ladders[7][0] = 71;
		ladders[7][1] = 91;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		boolean oddRow = true;
		
		
		// I used a string builder (^^Above) Because the board will compile going from left to right on each row, but that's not how the snakes and
		// ladders board is layed out
		// The below code is for when the position is occupied by a player, if that is the case we will print the player instead of the number
		for (int row = ROWS-1; row >= 0; row--) {
			for (int col = 0; col < COLS; col++) {
				if (oddRow) {
					String pl = "";
					boolean occupied = false;
					for (Player temp : playerPositions.keySet()) {
						if (playerPositions.get(temp) == gameBoard[row][COLS-1-col]) {
							occupied = true;
							pl += temp + " ";
						}
					}
					if (occupied) {
						// The below code will print the players if that location is occupied by players
						pl += "\t";
						sb.append(pl);
					} else {
						sb.append(gameBoard[row][COLS-1-col] + "\t");
					}
				} else {
					boolean occupied = false;
					String pl = "";
					for (Player temp : playerPositions.keySet()) {
						if (playerPositions.get(temp) == gameBoard[row][col]) {
							occupied = true;
							pl += (temp + " ");
						}
					}
					if (occupied) {
						pl += "\t";
						sb.append(pl);
					} else {
						sb.append(gameBoard[row][col] + "\t");
					}
				}
			}
			// The below code switches the oddRow flag and prints a new line
			oddRow = !oddRow;
			sb.append("\n");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
