package GameBoard;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Snakes {

	public static void main(String[] args) {
		//Below is the welcome message
		System.out.println("Welcome to Snakes & Ladders Console edition!");
		
		//Initialise scanner
		Scanner scan = new Scanner (System.in);
		
		//Prompt for number of players
		int numPlayers = 0;
		while (numPlayers <= 0 || numPlayers >6 ) {
			System.out.print("Enter number of player (1-6): " );
			numPlayers = scan.nextInt();
		}
		//Initialise players
		List<Player> players = new ArrayList<Player>();
		for (int idx = 0; idx < numPlayers; idx++) {
			Player player = new Player("p" + idx);
			players.add(player);
		}
		// Initialise board
		Board board = new Board(players);
		
		// The below code will loop until the counter reaches final number
		
		boolean done = false;
		int playerIdx = 0;
		while (!done) {
			Player currPlayer = players.get(playerIdx);
			int roll = currPlayer.takeTurn();
			
			//Printing the board
			System.out.println(board);
			System.out.println("-------------------------------\n");
			
			if (done) {
				System.out.println(currPlayer + " wins");
			}
			// Next Player
			playerIdx++;
			if (playerIdx == numPlayers) {
				playerIdx = 0;
			}
			
		}
		
	}

	private static void takeTurn() {
		// TODO Auto-generated method stub
		
	}
}
