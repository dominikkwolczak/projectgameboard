package GameBoard;

public class Player {
	private String name;
	private static Die die;
	
	// Initialise fields
	public Player(String name) {
		die = new Die();
		this.name = name;
	}
	
	public int takeTurn() {
		int roll = die.rollD6();
		System.out.println(name + " rolled " + roll + ".");
		return roll;

	//public Player(String string) {
		// TODO Auto-generated constructor stub
	}
public String toString() {
	return name;
	
}
}
